﻿using System.Text;
using TMPro;
using UnityEngine;

namespace Pandora.SceneView
{
    public class CameraInfo : MonoBehaviour
    {
        public TextMeshProUGUI StatusLabel;
        public AdvancedCamera DataContext;
        
        private StringBuilder Buf = new StringBuilder();

        public TextMeshProUGUI ManObject;
        public TextMeshProUGUI InputText;
       
        
        void Update()
        {
            if (DataContext == null)
            {
                StatusLabel.text = "<color=red>Camera controller unassigned!</color>";
                if(ManObject == null)
                    Buf.AppendLine("<color=red>Man label object is unassigned!</color>\n");
                return;
            }

            DisplayState();
            DisplayInputData();
            
            if (Input.GetKeyDown(KeyCode.O)) InputText.enabled = !InputText.enabled;
            if (Input.GetKeyDown(KeyCode.I)) ManObject.enabled = !ManObject.enabled;
            
        }

        private void DisplayState()
        {
            Buf.Clear();

            Buf.AppendFormat("Flight Speed: {0}m/s\n", DataContext.FlightSpeed);

            if (DataContext.Cam.orthographic)
                Buf.AppendFormat("OrthoScale: {0}\n", DataContext.Cam.orthographicSize);
            else
                Buf.AppendFormat("FOV: {0:F2}\n", DataContext.Cam.fieldOfView);

            Buf.AppendFormat("Location: {0}\n", DataContext.transform.position);

            var euler = DataContext.transform.rotation.eulerAngles;
            Buf.AppendFormat("Rotation: ({0:F2}°, {1:F2}°, {2:F2}°)\n", euler.x, euler.y, euler.z);
            Buf.AppendFormat("Mode: {0}\n", DataContext.Mode);

            StatusLabel.text = Buf.ToString();
        }

        private void DisplayInputData()
        {
            if(InputText.enabled == false) return;

            Buf.Clear();
            
            if (DataContext.Mode == CameraMode.Flight)
            {
                var data = DataContext.FlightData;
                
                Buf.AppendLine("<b>Flight input data</b>");
                Buf.AppendFormat("Right = {0}\n", data.Right);
                Buf.AppendFormat("Forward = {0}\n", data.Forward);
                Buf.AppendFormat("Up = {0}\n", data.Up);
                Buf.AppendFormat("RotZ = {0}\n", data.RotZ); 
                Buf.AppendFormat("RotX = {0}\n", data.RotX); 
                Buf.AppendFormat("Wheel = {0}\n", data.Wheel);
                Buf.AppendFormat("Speedup = {0}\n", data.Speedup); 
                Buf.AppendFormat("Zoom = {0}\n", data.Zoom);                
            }

            if (DataContext.Mode == CameraMode.Turntable)
            {
                var data = DataContext.TableData;
                
                Buf.AppendLine("<b>Turntable input data</b>");
                
                Buf.AppendFormat("Ctrl =  {0}\n", data.Ctrl);
                Buf.AppendFormat("Shift = {0}\n", data.Shift);
                Buf.AppendFormat("Alt =   {0}\n", data.Alt);
                Buf.AppendFormat("LMB =   {0}\n", data.LMB);
                Buf.AppendFormat("RMB =   {0}\n", data.RMB);
                Buf.AppendFormat("MMB =   {0}\n", data.MMB);
                Buf.AppendFormat("F =     {0}\n", data.F);
                Buf.AppendFormat("Z =     {0}\n", data.Z);
                Buf.AppendFormat("Mouse = ({0:F}, {1:F})\n", data.MouseDelta.x, data.MouseDelta.y);
                Buf.AppendFormat("Num 1 = {0}\n", data.Num1);
                Buf.AppendFormat("Num 3 = {0}\n", data.Num3);
                Buf.AppendFormat("Num 5 = {0}\n", data.Num5);
                Buf.AppendFormat("Num 7 = {0}\n", data.Num7);
                Buf.AppendFormat("Num 9 = {0}\n", data.Num9);
            }
            
            InputText.text = Buf.ToString();
        }

        private void Start()
        {
            if (DataContext == null)
            {
                DataContext = Camera.main.GetComponent<AdvancedCamera>();
            }
        }
    }
}