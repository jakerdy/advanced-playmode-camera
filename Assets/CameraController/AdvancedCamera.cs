﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pandora.SceneView
{
    public enum CameraMode
    {
        Turntable,
        Flight
    }

    public class AdvancedCamera : MonoBehaviour
    {
        #region Vars

        public bool CanSelect { get; set; }
        public CameraMode Mode { get; private set; }
        
        [Header("Refs")]
        public Camera Cam;
        public SceneSelectionManager SelectionManager;
        
        [Header("General")]
        private float mCameraFOV;
        public float CameraFOV
        {
            get { return mCameraFOV; }
            set { mCameraFOV = Mathf.Clamp(value, 1f, 179f); }
        }
        public float DefaultFOV = 60;
        public float DefaultOrthoSize = 5f;
        public float SpeedUpFactor = 2;
        public Vector3 HomePosition = Vector3.one;
        public Vector3 HomeForward = -Vector3.one;   
        
        [Header("Fly Mode")] 
        public List<float> SpeedValues = new List<float> {
            0.01f,
            0.05f,
            0.1f,
            0.5f,
            1.0f,
            2.5f,
            5.0f,
            10.0f,
            15.0f,
            20.0f,
            30.0f,
            40.0f,
            50.0f,
            75.0f,
            100.0f,
            200.0f,
            500.0f,
        };
        public int DefaultSpeedIndex = 7;
        
        public float FlightSpeed { get; private set; } = 1f; 
        public float RotationSpeed = 2f;
        public float MagnificationSpeed = 0.5f;
        
        [Header("Turntable")] 
        public float DollySpeed = 0.2f;
        public float PanSpeed = 20f;
        public float OrbitSpeed = 2f;
        public float OrthoSpeed = 0.2f;
        public float FovSpeed = 1f;
        public Vector3 OrbitPoint { get; private set; }
        
        [Header("Tweaks")] 
        public float ZoomResetDecaySpeed = 4f;
        public float DefaultOrbitDistance = 10f;

        #endregion
        
        
        private float TempFov; //For correct reset after returning from flight mode
        void LateUpdate()
        {
            bool Alt = Input.GetKey(KeyCode.LeftAlt);
            bool Ctrl = Input.GetKey(KeyCode.LeftControl);
            bool Shift = Input.GetKey(KeyCode.LeftShift);
            
            if (Input.GetMouseButtonDown(1) && (Alt == false) && ((Ctrl || Shift) == false))
            {
                TempFov = CameraFOV;
                Mode = CameraMode.Flight;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }

            if (Input.GetMouseButtonUp(1) && Alt == false)
            {
                if(Mode == CameraMode.Flight) CameraFOV = TempFov;
                Mode = CameraMode.Turntable;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }

            CanSelect = false;
            
            switch (Mode)
            {
                case CameraMode.Flight:
                {
                    ProcessFlight(); 
                    break;
                }
                case CameraMode.Turntable:
                {
                    ProcessTurntable(); 
                    break;
                }
            }

            ProcessBookmarks();
            
            Cam.fieldOfView = Mathf.Lerp(Cam.fieldOfView, CameraFOV, Time.deltaTime * ZoomResetDecaySpeed);
            
        }

        #region Flight

        #region KeyMappings

        //TODO: Add settings for key mappings

        #endregion
        
        public class InputFlightData
        {
            public float Right;
            public float Up;
            public float Forward;
            
            public float RotZ;
            public float RotX;
            public float Wheel;
            public float Speedup;
            public float Zoom;

            public void UpdateValues(float SpeedUpFactor)
            {
                Right = GetAxisFromKeys(KeyCode.A, KeyCode.D);
                Forward = GetAxisFromKeys(KeyCode.S, KeyCode.W);
                Up = GetAxisFromKeys(KeyCode.Q, KeyCode.E);
                
                //Up = Input.GetAxis("Elevation");

                RotZ = Input.GetAxis("Mouse X");
                RotX = -Input.GetAxis("Mouse Y");
            
                Wheel =  Input.GetAxis("Mouse ScrollWheel");
                if (Wheel != 0) Wheel = Wheel > 0 ? 1 : -1;

                Speedup = (Input.GetAxis("Fire3") + 1) * (SpeedUpFactor - 1);

                Zoom = GetAxisFromKeys(KeyCode.R, KeyCode.F);
            }

            private float GetAxisFromKeys(KeyCode neg, KeyCode pos)
            {
                float negValue = Input.GetKey(neg) ? -1 : 0;
                float posValue = Input.GetKey(pos) ? 1 : 0;
                return negValue + posValue;
            }
        }

        public InputFlightData FlightData = new InputFlightData();

        private int SpeedIndex = 0;
        
        void ProcessFlight()
        {
            FlightData.UpdateValues(SpeedUpFactor);

            float responce = Mathf.Pow(CameraFOV - 0.975f, 0.3f) * 0.35f;
            //FOV
            CameraFOV += FlightData.Zoom * MagnificationSpeed * Time.deltaTime * responce;
            
            
            //Flight speed
            SpeedIndex = Mathf.Clamp(Mathf.RoundToInt(SpeedIndex + FlightData.Wheel), 0, SpeedValues.Count-1);
            
            
            FlightSpeed = SpeedValues[SpeedIndex] * FlightData.Speedup;
            
            float Speed = FlightSpeed * Time.deltaTime;

            float OrbitDist = (OrbitPoint - transform.position).magnitude;
            
            //Movement
            transform.position += transform.right   * (FlightData.Right   * Speed) + 
                                  transform.up      * (FlightData.Up      * Speed) +
                                  transform.forward * (FlightData.Forward * Speed);
            
            //Rotation
            Speed = RotationSpeed * responce;
            
            transform.Rotate(transform.right, FlightData.RotX * Speed, Space.World);
            transform.Rotate(Vector3.up     , FlightData.RotZ * Speed, Space.World);

            OrbitPoint = transform.position + transform.forward * OrbitDist;
        }
        
        #endregion

        #region TurnTable

        public class InputTurntableData
        {
            public bool Ctrl;
            public bool Alt;
            public bool Shift;

            public bool F, Z;
            public bool Num1, Num3, Num5, Num7, Num9;

            public bool LMB, RMB, MMB;
            public Vector2 MouseDelta;
            
            public void UpdateValues()
            {
                Ctrl =  Input.GetKey(KeyCode.LeftControl);
                Alt =   Input.GetKey(KeyCode.LeftAlt);
                Shift = Input.GetKey(KeyCode.LeftShift);

                F = Input.GetKeyDown(KeyCode.F);
                Z = Input.GetKeyDown(KeyCode.Z);

                Num1 = Input.GetKeyDown(KeyCode.Keypad1);
                Num3 = Input.GetKeyDown(KeyCode.Keypad3);
                Num5 = Input.GetKeyDown(KeyCode.Keypad5);
                Num7 = Input.GetKeyDown(KeyCode.Keypad7);
                Num9 = Input.GetKeyDown(KeyCode.Keypad9);

                LMB = Input.GetMouseButton(0);
                RMB = Input.GetMouseButton(1);
                MMB = Input.GetMouseButton(2);
                
                MouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
            }
        }

        public InputTurntableData TableData = new InputTurntableData();

        private float OrbitPointDistance;// => Vector3.Dot(transform.forward, OrbitPoint - transform.position);

        void ProcessTurntable()
        {
            TableData.UpdateValues();

            //Pan, Orbit, Dolly
            if (TableData.Alt == true)
            {
                //Orbit
                if (TableData.LMB == true)
                {
                    float Speed = OrbitSpeed;
                    transform.Rotate(transform.right, -TableData.MouseDelta.y * Speed, Space.World);
                    transform.Rotate(Vector3.up     ,  TableData.MouseDelta.x * Speed, Space.World);

                    if (OrbitPointDistance < 0)
                    {
                        OrbitPoint = transform.position + transform.forward;
                        OrbitPointDistance = 1;
                    }

                    transform.position = OrbitPoint - transform.forward * OrbitPointDistance;
                    
                    return;
                }

                OrbitPointDistance = Vector3.Dot(transform.forward, OrbitPoint - transform.position); 

                //Dolly
                if (TableData.RMB == true && Cam.orthographic == false)
                {   
                    float delta = DollySpeed * (TableData.MouseDelta.x + TableData.MouseDelta.y);
                    transform.position += transform.forward * delta;
                    return;
                }

                //Pan
                if (TableData.MMB == true)
                {
                    float sign = Mathf.Sign(Vector3.Dot(transform.forward, OrbitPoint - transform.position));
                    
                    var screenMovement = (Cam.WorldToScreenPoint(OrbitPoint) + (Vector3) TableData.MouseDelta);
                    var delta = PanSpeed * sign * (Cam.ScreenToWorldPoint(screenMovement) - OrbitPoint);
                    
                    transform.position -= delta;
                    OrbitPoint -= delta;
                    
                    return;
                }
            }
            
            //Fov
            if (TableData.Ctrl && TableData.Shift && TableData.RMB)
            {
                float delta = -(TableData.MouseDelta.x + TableData.MouseDelta.y);

                if (Cam.orthographic)
                {
                    float size = Cam.orthographicSize + OrthoSpeed * delta;
                    Cam.orthographicSize = size <= 0.0001f ? 0.01f : size;
                }
                else
                {
                    float responce = Mathf.Pow(CameraFOV - 0.9f, 0.25f);
                    CameraFOV += delta * responce * FovSpeed;
                }

                return;
            }

            CanSelect = true;
            
            //Reset FOV to default value
            if (TableData.Shift && TableData.Z)
            {
                if (Cam.orthographic)
                {
                    Cam.orthographicSize = DefaultOrthoSize;    
                }
                else
                {
                    CameraFOV = DefaultFOV;    
                }
            }

            //Go Home
            if (TableData.Shift && TableData.F)
            {
                transform.position = HomePosition;
                transform.forward = HomeForward;
                return;
            }
            
            //Frame selected
            int SelectionCount = SelectionManager.SelectedObjects.Count;
            if (TableData.F == true && SelectionCount > 0)
            {
                Vector3 CenterOfMass = Vector3.zero;

                for (int i = 0; i < SelectionCount; i++)
                {
                    CenterOfMass += SelectionManager.SelectedObjects[i].transform.position;
                }

                CenterOfMass /= SelectionCount;

                OrbitPoint = CenterOfMass;

                float Radius = 0;

                //Find most far object
                for (int i = 0; i < SelectionCount; i++)
                {
                    var selectedObj = SelectionManager.SelectedObjects[i].transform;
                    float tempRad = (selectedObj.position - CenterOfMass).magnitude;
                    
                    var rend = selectedObj.GetComponent<Renderer>();
                    if (rend != null) tempRad += rend.bounds.size.magnitude;
                    
                    if (tempRad > Radius) Radius = tempRad;
                }

                float angle = (180 - 90 - (CameraFOV/2)) * Mathf.Deg2Rad;

                float Dist = Radius * Mathf.Tan(angle);

                if (Cam.orthographic) Cam.orthographicSize = Radius * 1.2f;
                
                transform.position = OrbitPoint - transform.forward * Dist;
            }
            
            
            //Persp/Ortho
            if (TableData.Num5)
            {
                Cam.orthographic = !Cam.orthographic;
            }
            
            //Front
            if (TableData.Num1)
            {
                float Dist = (transform.position - OrbitPoint).magnitude;
                
                //Back
                if (TableData.Ctrl)
                {
                    transform.position = OrbitPoint + Vector3.back * Dist;
                    transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
                }
                //Front
                else
                {
                    transform.position = OrbitPoint + Vector3.forward * Dist;
                    transform.rotation = Quaternion.LookRotation(Vector3.back, Vector3.up);
                }
            }
            
            //Right
            if (TableData.Num3)
            {
                float Dist = (transform.position - OrbitPoint).magnitude;
                
                //Left
                if (TableData.Ctrl)
                {
                    transform.position = OrbitPoint + Vector3.left * Dist;     
                    transform.rotation = Quaternion.LookRotation(Vector3.right, Vector3.up);
                }
                //Right
                else
                {
                    transform.position = OrbitPoint + Vector3.right * Dist;                 
                    transform.rotation = Quaternion.LookRotation(Vector3.left, Vector3.up);
                }
            }
            
            //Top / Bottom
            if (TableData.Num7)
            {
                float Dist = (transform.position - OrbitPoint).magnitude;
                
                //Bottom
                if (TableData.Ctrl)
                {
                    transform.position = OrbitPoint + Vector3.down * Dist;
                    transform.rotation = Quaternion.LookRotation(Vector3.up, Vector3.forward);
                }
                //Top
                else
                {
                    transform.position = OrbitPoint + Vector3.up * Dist;                 
                    transform.rotation = Quaternion.LookRotation(Vector3.down, Vector3.forward);                    
                }
            }

            //Rotate arround 180°
            if (TableData.Num9)
            {
                transform.position = transform.position + (OrbitPoint - transform.position) * 2;
                transform.forward = -transform.forward;
            }
        }

        #endregion

        #region BookMarks

        public class InputBookmarkData
        {
            public bool Ctrl;
            public bool Shift;
            public int Bookmark;

            public void UpdateValues()
            {
                Ctrl = Input.GetKey(KeyCode.LeftControl);
                Shift = Input.GetKey(KeyCode.LeftShift);

                if (Input.GetKeyDown(KeyCode.Alpha0)) Bookmark = 0;

                Bookmark = -1;
                int i, k = 0;
                
                for (i = (int) KeyCode.Alpha0; i < (int) KeyCode.Alpha9; i++, k++)
                {
                    if(Input.GetKeyDown((KeyCode)i)) Bookmark = k;
                }
            }
        }

        private InputBookmarkData BookmarkInput = new InputBookmarkData();
        
        [Serializable]
        public struct LocationBookmark
        {
            public Quaternion Rotation;
            public Vector3 Position;
            public float FOV;
            public bool Ortho;
        }

        public List<LocationBookmark> Bookmarks = Enumerable.Repeat(
                new LocationBookmark()
                {
                    Ortho = false,
                    FOV = 60,
                    Position = Vector3.zero, 
                    Rotation = Quaternion.identity
                }, 10).ToList();

        public void ProcessBookmarks()
        {
            BookmarkInput.UpdateValues();
            
            //Skip if not hotkeyed
            if(BookmarkInput.Bookmark < 0) return;
            Debug.Log($"Bookmark {BookmarkInput.Bookmark}");
            
            
            //Store
            if (BookmarkInput.Ctrl && BookmarkInput.Shift)
            {
                Bookmarks[BookmarkInput.Bookmark] = new LocationBookmark
                {
                    Ortho = Cam.orthographic,
                    FOV = Cam.orthographic ? Cam.orthographicSize : CameraFOV,
                    Position = transform.position,
                    Rotation = transform.rotation
                };
                return;
            }
            
            //GoTo
            if (BookmarkInput.Ctrl == true)
            {
                var bookmark = Bookmarks[BookmarkInput.Bookmark];

                Cam.orthographic = bookmark.Ortho;

                if (bookmark.Ortho) Cam.orthographicSize = bookmark.FOV;
                else CameraFOV = bookmark.FOV;
                
                transform.position = bookmark.Position;
                transform.rotation = bookmark.Rotation;
            }
        }
        
        #endregion
        
        #region MonoBHV
        
        void Start()
        {
            //TODO: Bind to settings
            CameraFOV = DefaultFOV;
            SpeedIndex = DefaultSpeedIndex;
            FlightSpeed = SpeedValues[SpeedIndex];
            OrbitPoint = transform.position + transform.forward * DefaultOrbitDistance;
            if (SelectionManager == null) SelectionManager = FindObjectOfType<SceneSelectionManager>();
            if(SelectionManager == null) Debug.LogError("There is no selection manager in scene!");
        }
        
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, transform.position + transform.forward * 2);
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, transform.position + transform.up * 2);
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, transform.position + Vector3.up * 2);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + transform.right * 2);

            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(OrbitPoint, 0.2f);
        }

        #endregion
    }
}