﻿using System.Collections.Generic;
using UnityEngine;

namespace Pandora.SceneView
{
    public class SceneSelectionManager : MonoBehaviour
    {
        public List<BBSelector> SelectorsPool = new List<BBSelector>();
        public List<GameObject> SelectedObjects = new List<GameObject>();

        public BBSelector SelectorTemplate;

        private bool Mulitselect = false;

        public AdvancedCamera RaycastCam;
        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.LeftControl)) Mulitselect = true;
            if (Input.GetKeyUp(KeyCode.LeftControl)) Mulitselect = false;

            if (Input.GetMouseButtonUp(0) && RaycastCam.CanSelect)
            {
                if (Mulitselect == false)
                {
                    DeselectAll();
                }

                var ray = RaycastCam.Cam.ScreenPointToRay(Input.mousePosition);

                RaycastHit HitInfo;
                Physics.Raycast(ray, out HitInfo);

                if (HitInfo.transform != null && SelectedObjects.Contains(HitInfo.transform.gameObject) == false)
                {
                    var renderer = HitInfo.transform.GetComponent<Renderer>();
                    if (renderer != null)
                    {
                        var Selector = GetSelector();
                        Selector.Select(renderer);
                        SelectedObjects.Add(renderer.gameObject);
                    }
                }
            }
        }

        private BBSelector GetSelector()
        {
            if (SelectedObjects.Count == SelectorsPool.Count)
            {
                var inst = Instantiate(SelectorTemplate, transform);
                SelectorsPool.Add(inst);
            }

            var Selector = SelectorsPool[SelectedObjects.Count];
            Selector.gameObject.SetActive(true);
            return Selector;
        }
        
        private void DeselectAll()
        {
            SelectedObjects.Clear();
            for (var i = 0; i < SelectorsPool.Count; i++)
            {
                SelectorsPool[i].gameObject.SetActive(false);
            }
        }
    }
}