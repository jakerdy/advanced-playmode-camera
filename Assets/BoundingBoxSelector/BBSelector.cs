﻿using UnityEngine;

namespace Pandora.SceneView
{
    public class BBSelector : MonoBehaviour
    {
        public GameObject SelectedObj;
    
        public void Select(Renderer rend)
        {
            var bounds = rend.bounds;
            transform.position = bounds.center;
            transform.localScale = bounds.extents * (2.0f * 1.01f);

            SelectedObj = rend.gameObject;
        }
    }
}
